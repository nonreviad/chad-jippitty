# Chad Jippitty

This browser extension changes all instances of 'Chat GPT' to 'Chad Jippitty'. That's it, that's all it does.

## Building

```
npm run build
```

## Installing

Just build, then look up how you can load the `dist` directory in your browser extensions manager (you'll need to enable developer mode or something). Should support all Chromium and Firefox based browsers that support Manifest V3.