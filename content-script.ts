const regex = /[cC][hH][aA][tT][ ]*[gG][pP][tT]/g
const renameStuff = (node: Node) => {
    if (node.nodeType == Node.TEXT_NODE && !node.hasChildNodes()) {
        const originalContent = node.textContent
        if (originalContent != null) {
            const changed = originalContent.replaceAll(regex, "Chad Jippitty")
                if (changed != originalContent) {
                    node.textContent = changed
                }
        }
    } else if(node.hasChildNodes()) {
        for (let child_idx = 0; child_idx < node.childNodes.length; ++child_idx) {
            renameStuff(node.childNodes[child_idx])
        }
    }
}
const mo = new MutationObserver((mutations: MutationRecord[])=>{
    for (let mutation_idx = 0; mutation_idx < mutations.length; ++mutation_idx) {
        const mutation = mutations[mutation_idx]
        const added = mutation.addedNodes
        for (let added_idx = 0; added_idx < added.length; ++added_idx) {
            const added_node = added[added_idx]
            renameStuff(added_node)
        }
    }
})
mo.observe(document.documentElement, {childList: true, subtree: true})
// Delete all existing stuff.
renameStuff(document.documentElement)